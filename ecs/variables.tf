variable "aws_region" {
  type = string
}
variable "env" {
  type = string
}

variable "aws_account_id" {
  type = number
}

variable "ecr_repository_name" {
  type = string
}

variable "ecs_cluster_name" {
  type = string
}

variable "ecs_service_name" {
  type = string
}

variable "desired_count" {
  type = number
}

variable "ecs_task_family" {
  type = string
}

variable "ecs_task_container_name" {
  type = string
}

variable "log_group_name" {
  type = string
}

variable "log_stream_prefix" {
  type = string
}

variable "aws_lambda_s3" {
  type = string
}

variable "cohort_trigger_s3_bucket_name" {
  type = string
}

variable "coredb_user_secret_arn" {
  type = string
}

variable "rds_vpc_id" {
  type = string
}

variable "ecs_subnet_cidr" {
  type = string
}

variable "ecs_task_security_group_name" {
  type = string
}

variable "ecs_task_role_name" {
  type = string
}

variable "ecs_task_policy_name" {
  type = string
}

variable "ecs_task_execution_role_name" {
  type = string
}
variable "policy_file_path" {
  type = string
}

# cloudwatch variables
variable "sns_topic_arn" {
  description = "ARN of the SNS topic to send notifications"
  type = string
}

variable "cpu_threshold" {
  description = "CPU threshold for the alarm"
}
variable "alarm_name" {
  type = string
}
