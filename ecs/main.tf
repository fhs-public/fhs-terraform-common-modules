resource "aws_ecr_repository" "repository" {
  name = var.ecr_repository_name
}

resource "aws_ecs_cluster" "cluster" {
  name = var.ecs_cluster_name
}

data "aws_vpc" "rds_vpc" {
  id = var.rds_vpc_id
}

data "aws_availability_zones" "available_zones" {
  state = "available"
}
resource "aws_subnet" "ecs_subnet" {
  vpc_id                  = var.rds_vpc_id
  cidr_block              = var.ecs_subnet_cidr
  availability_zone       = data.aws_availability_zones.available_zones.names[0]
  map_public_ip_on_launch = true
}

data "aws_internet_gateway" "default" {
  filter {
    name   = "attachment.vpc-id"
    values = [data.aws_vpc.rds_vpc.id]
  }

  filter {
    name   = "attachment.state"
    values = ["available"]
  }
}

resource "aws_route_table" "ecs_route_table" {
  vpc_id = var.rds_vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = data.aws_internet_gateway.default.id
  }
}

resource "aws_route_table_association" "ecs_route_table_association" {
  subnet_id      = aws_subnet.ecs_subnet.id
  route_table_id = aws_route_table.ecs_route_table.id
}

resource "aws_security_group" "sg_task" {
  name   = var.ecs_task_security_group_name
  vpc_id = var.rds_vpc_id

  ingress {
    protocol         = "tcp"
    from_port        = 5000
    to_port          = 5000
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_ecs_service" "service" {
  name            = var.ecs_service_name
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.task.arn
  launch_type     = "FARGATE"
  desired_count   = var.desired_count

  network_configuration {
    security_groups  = [aws_security_group.sg_task.id]
    subnets          = [aws_subnet.ecs_subnet.id]
    assign_public_ip = true
  }

  deployment_circuit_breaker {
    enable   = true
    rollback = true
  }
}

resource "aws_cloudwatch_log_group" "log-group" {
  name = var.log_group_name
}

resource "aws_cloudwatch_log_stream" "log-stream" {
  name           = var.log_stream_prefix
  log_group_name = aws_cloudwatch_log_group.log-group.name
}

resource "aws_ecs_task_definition" "task" {
  family                   = var.ecs_task_family
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = 16384
  cpu                      = 4096
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn            = aws_iam_role.ecs_task_role.arn
  container_definitions    = <<DEFINITION
[
  {
    "name": "${var.ecs_task_container_name}",
    "image": "${aws_ecr_repository.repository.repository_url}:latest",
    "entryPoint": [ "python3.8" ],
    "command": ["-u", "handler.py", "csv_importer"],
    "essential": true,
    "cpu": 4096,
    "memory": 16384,
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
            "awslogs-group": "${aws_cloudwatch_log_group.log-group.name}",
            "awslogs-region": "${var.aws_region}",
            "awslogs-stream-prefix": "${aws_cloudwatch_log_stream.log-stream.name}"
        }
    },
    "portMappings": [
      {
        "containerPort": 5000,
        "hostPort": 5000
      }
    ]
  }
]
DEFINITION
}

resource "aws_iam_role" "ecs_task_role" {
  name = var.ecs_task_role_name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
        "Effect": "Allow",
        "Principal": {
         "Service": "ecs-tasks.amazonaws.com"
        },
            "Action": "sts:AssumeRole",
            "Condition": {
                "StringEquals": {
                    "aws:SourceAccount": "${var.aws_account_id}"
                }
            }    }    
  ]
}
EOF
}

data "local_file" "policy" {
  filename = var.policy_file_path
  
}
resource "aws_iam_policy" "ecs_permissions" {
  name        = var.ecs_task_policy_name
  policy = data.local_file.policy.content
}



resource "aws_iam_role_policy_attachment" "ecs_attachment" {
  role       = aws_iam_role.ecs_task_role.name
  policy_arn = aws_iam_policy.ecs_permissions.arn
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = var.ecs_task_execution_role_name
  assume_role_policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "",
			"Effect": "Allow",
			"Principal": {
				"Service": "ecs-tasks.amazonaws.com"
			},
            "Action": "sts:AssumeRole",
            "Condition": {
                "StringEquals": {
                    "aws:SourceAccount": "${var.aws_account_id}"
                }
            }    }  		
	]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}
##Cloudwatch_alert
resource "aws_cloudwatch_metric_alarm" "cpu_utilization_alarm" {
  alarm_name          = var.alarm_name
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  metric_name         = "CpuUtilized"
  namespace           = "ECS/ContainerInsights"
  period              = 300  #2.5min
  statistic           = "Average"
  threshold           = var.cpu_threshold
  alarm_description   = "High CPU utilization detected ${var.cpu_threshold}%"
  insufficient_data_actions = []
  actions_enabled = true
  ok_actions = []
  dimensions = {
    ClusterName = var.ecs_cluster_name, 
  }
  alarm_actions = [
    var.sns_topic_arn
  ]
  tags = {
    Name = var.alarm_name
  }
}
